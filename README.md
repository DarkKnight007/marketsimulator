## Market Simulator
Market Simulator has below main classes/components.

**SimulatorApplication** starts the application. It has createReferenceData() method which creates Stocks and Clients and saves to H2 Database.

**model** folder conatins Client, Stock and Trade POJO Classes

**TradeGenerator** has generateTrades() method which contains the logic to generate trades randomly.

**TradePublisher** mimics the trade source by publishing trades randomly

**TradeQueue** holds the trade published by TradePublisher.

**TradeProcessor** reads the trades from TradeQueue and process it. When all the trades are processed it published the blocks to downstream i.e. src/main/resources/blocks.txt

**TradeHandler** will save the trade to H2 database and publish executed block to console

---

## Compile and Run the application
1. Go to **Simulator** folder 
2. Assuming that maven is installed on the machine, run below command to create package
   **mvn clean package**
3. After package is created successfully, run below command to start the program
   **java -jar target\simulator-0.0.1-SNAPSHOT.jar**
4. Press any key to stop execution of program after the console stops updating the status
5. The text files for downstream is generated at **src/main/resources/blocks.txt**


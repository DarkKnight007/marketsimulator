package com.trading.simulator.repository;

import com.trading.simulator.model.Trade;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Created by bhushaningle on 8/2/2019.
 */
@Repository
public interface TradeRepository extends JpaRepository<Trade,Long> {

    @Query(value = "SELECT t.* FROM Trade t INNER JOIN Stock S ON T.StockId = S.StockId AND T.StockId=?1 " +
            "INNER JOIN Client C ON T.ClientId = C.ClientId AND T.ClientId=?2 WHERE t.IsBuy=?3 ",
            nativeQuery = true)
    Optional<List<Trade>> getTradesByStockClientAndIsBuy(Long StockId, Long ClientId, boolean isBuy);

}

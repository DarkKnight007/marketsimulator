package com.trading.simulator.repository;

import com.trading.simulator.model.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by bhushaningle on 8/2/2019.
 */
@Repository
public interface ClientRepository extends JpaRepository<Client,Long> {
}


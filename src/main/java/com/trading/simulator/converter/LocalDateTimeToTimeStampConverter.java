package com.trading.simulator.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.time.LocalDateTime;

/**
 * Created by bhushaningle on 8/2/2019.
 */
@Converter(autoApply = true)
public class LocalDateTimeToTimeStampConverter implements AttributeConverter<LocalDateTime, java.sql.Timestamp> {

    @Override
    public java.sql.Timestamp convertToDatabaseColumn(LocalDateTime entityAttribute) {
        return entityAttribute == null ? null : java.sql.Timestamp.valueOf(entityAttribute);
    }

    @Override
    public LocalDateTime convertToEntityAttribute(java.sql.Timestamp databaseColumn) {
        return databaseColumn == null ? null : databaseColumn.toLocalDateTime();
    }


}

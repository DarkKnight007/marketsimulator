package com.trading.simulator.service;

import com.trading.simulator.model.Trade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by bhushaningle on 8/2/2019.
 */
@Component
public class TradeProcessor {
    private static Logger logger = LoggerFactory.getLogger(TradeProcessor.class);
    private TradeHandler tradeHandler;
    private TradeQueue tradeQueue;
    public final Runnable processTrade = () -> {
        try {
            while (true) {
                Trade trade = tradeQueue.getTrade();
                if (trade.getQuantity() == 0) {
                    logger.info("Market Closed");
                    break;
                }
                tradeHandler.handleTrade(trade);
            }
            tradeHandler.publishBlocksToDownStream();
        } catch (InterruptedException ie) {
            logger.error("Error Occurred while processing trades");
            logger.error(ie.getMessage());
            Thread.currentThread().interrupt();
        }
    };

    @Autowired
    public TradeProcessor(TradeQueue tradeQueue, TradeHandler tradeHandler) {
        this.tradeQueue = tradeQueue;
        this.tradeHandler = tradeHandler;
    }
}


package com.trading.simulator.service;

import com.trading.simulator.model.Trade;
import org.springframework.stereotype.Component;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by bhushaningle on 8/2/2019.
 */

@Component
public class TradeQueue  {

    BlockingQueue<Trade> tradeQueue = new LinkedBlockingQueue<Trade>();

    public Trade getTrade() throws InterruptedException{
        return tradeQueue.take();
    }

    public void putTrade(Trade trade) throws InterruptedException{
        tradeQueue.put(trade);
    }
}

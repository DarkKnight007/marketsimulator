package com.trading.simulator.service;

import com.trading.simulator.model.Trade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;

/**
 * Created by bhushaningle on 8/2/2019.
 */
@Component
public class TradeHandler {
    private static final Function<Trade, List<Object>> BLOCK_COMPOSITE_KEY = trade -> {
        return Arrays.asList(trade.getStock(), trade.getClient(), trade.isBuy());
    };
    private static Logger logger = LoggerFactory.getLogger(TradeHandler.class);
    private TradeService tradeService;

    @Autowired
    public TradeHandler(TradeService tradeService) {
        this.tradeService = tradeService;
    }

    public void handleTrade(Trade trade) {
        tradeService.saveTrade(trade);
        publishBlock(trade);
    }

    private void publishBlock(Trade trade) {
        Optional<List<Trade>> blockTrades = tradeService.getBlockTrades(trade);
        if (blockTrades.isPresent()) {
            List<Trade> trades = blockTrades.get();
            logger.info(String.format("Executed Block:: StockCode:%s | ClientCode:%s | Side:%s | ExecutedQuantity:%s",
                    trade.getStock().getStockCode(), trade.getClient().getClientCode(), trade.getSide(), getExecutedQty(trades).toPlainString()));
        } else {
            logger.info("No Trades found for Trade : " + trade.toString());
        }
    }

    public void publishBlocksToDownStream() {
        logger.info("Publishing blocks to Down Stream");
        String filePath = "src/main/resources/blocks.txt";

        List<Trade> allTrades = tradeService.getAllTrades();
        Map<Object, List<Trade>> blocks = allTrades.stream().collect(groupingBy(BLOCK_COMPOSITE_KEY, Collectors.toList()));

        try (PrintWriter pw = new PrintWriter(Files.newBufferedWriter(Paths.get(filePath)))) {
            pw.println("StockCode|ClientCode|Side|ExecutedQuantity");
            blocks.forEach((key, value) -> {
                pw.println(getBlock(value));
            });
            logger.info("Completed Publishing blocks to downstream (src/main/resources/blocks.txt)");
        } catch (IOException ioe) {
            logger.error("Error Occurred while Publishing trades");
            logger.error(ioe.getMessage());
        }
    }

    private String getBlock(List<Trade> trades) {
        Optional<Trade> trade = trades.stream().findFirst();
        if (trade.isPresent()) {
            return String.format("%s|%s|%s|%f",
                    trade.get().getStock().getStockCode(),
                    trade.get().getClient().getClientCode(),
                    trade.get().getSide(),
                    getExecutedQty(trades));
        } else return "";
    }

    private BigDecimal getExecutedQty(List<Trade> trades) {
        return trades.stream().map(Trade::getAmount)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }
}

package com.trading.simulator.service;

import com.trading.simulator.model.Client;
import com.trading.simulator.model.Stock;
import com.trading.simulator.model.Trade;
import com.trading.simulator.repository.ClientRepository;
import com.trading.simulator.repository.StockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by bhushaningle on 8/2/2019.
 */
@Component
public class TradeGenerator {
    private final StockRepository stockRepository;
    private final ClientRepository clientRepository;

    @Autowired
    public TradeGenerator(StockRepository stockRepository, ClientRepository clientRepository) {
        this.stockRepository = stockRepository;
        this.clientRepository = clientRepository;
    }

    public final List<Trade> getTrades() {
        return generateTrades();
    }

    private final List<Trade> generateTrades() {
        final List<Trade> tradeList = new ArrayList<>();
        final Random random = new Random();
        final List<Stock> stocks = stockRepository.findAll();
        final List<Client> clients = clientRepository.findAll();

        for (int s = 0; s < stocks.size(); s++) {
            BigDecimal prevTradePrice = stocks.get(s).getClosingPrice();
            for (int c = 0; c < clients.size(); c++) {
                boolean isBuy = random.nextBoolean();
                BigDecimal price = isBuy ? prevTradePrice.add(BigDecimal.ONE) : prevTradePrice.subtract(BigDecimal.ONE);
                prevTradePrice = price;
                Trade t = new Trade(stocks.get(s), clients.get(c), isBuy, price,
                        random.ints(20, 110).findFirst().getAsInt());
                t.setEnteredDate(LocalDateTime.now());
                tradeList.add(t);
            }
        }
        return tradeList;
    }

    public final Trade getEmptyTrade() {
        Stock st = stockRepository.findOne(1l);
        Client cl = clientRepository.findOne(1L);
        return new Trade(st, cl, true, BigDecimal.ZERO, 0);
    }
}

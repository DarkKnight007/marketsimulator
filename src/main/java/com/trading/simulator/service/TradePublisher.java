package com.trading.simulator.service;

import com.trading.simulator.model.Trade;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Random;

/**
 * Created by bhushaningle on 8/2/2019.
 */

@Component
public class TradePublisher {
    private static Logger logger = LoggerFactory.getLogger(TradePublisher.class);
    private TradeGenerator tradeGenerator;
    private TradeQueue tradeQueue;

    public final Runnable publishTrades = () -> {
        try {

            final Random random = new Random();
            List<Trade> tradeList = tradeGenerator.getTrades();
            int noOfTrades = tradeList.size();
            for (int i = 0; i < noOfTrades; i++) {
                int randomTradeIndex = random.nextInt(tradeList.size());
                Trade t = tradeList.get(randomTradeIndex);
                tradeQueue.putTrade(t);
                logger.info(String.format("Published New trade {Stock:%s|Client:%s|Side:%s|Price:%f|Qty:%d}",
                        t.getStock().getStockCode(), t.getClient().getClientCode(), t.getSide(), t.getPrice(), t.getQuantity()));
                tradeList.remove(randomTradeIndex);
                Thread.sleep(random.ints(100, 500).findAny().getAsInt());
            }

            //Putting empty trade to simulate market closing
            tradeQueue.putTrade(tradeGenerator.getEmptyTrade());
            logger.info("Stopped Trade Publishing!");

        } catch (InterruptedException ie) {
            logger.error("Error while publishing trades " + ie.getMessage());
            Thread.currentThread().interrupt();
        }
    };

    @Autowired
    public TradePublisher(TradeQueue tradeQueue, TradeGenerator tradeGenerator) {
        this.tradeQueue = tradeQueue;
        this.tradeGenerator = tradeGenerator;
    }


}

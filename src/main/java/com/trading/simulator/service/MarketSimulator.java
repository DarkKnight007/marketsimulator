package com.trading.simulator.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by bhushaningle on 8/2/2019.
 */
@Component
public class MarketSimulator {

    private TradePublisher tradePublisher;
    private TradeProcessor tradeProcessor;

    @Autowired
    public MarketSimulator(TradePublisher tradePublisher, TradeProcessor tradeProcessor) {
        this.tradeProcessor = tradeProcessor;
        this.tradePublisher = tradePublisher;
    }

    public void startTrading() {
        new Thread(tradePublisher.publishTrades).start();
        new Thread(tradeProcessor.processTrade).start();
    }

}

package com.trading.simulator.service;

import com.trading.simulator.model.Trade;
import com.trading.simulator.repository.TradeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Created by bhushaningle on 8/2/2019.
 */
@Component
public class TradeService {

    private TradeRepository tradeRepository;

    @Autowired
    public TradeService(TradeRepository tradeRepository) {
        this.tradeRepository = tradeRepository;
    }

    @Transactional(readOnly = true)
    public List<Trade> getAllTrades() {
        return tradeRepository.findAll();
    }

    @Transactional
    public Trade saveTrade(Trade trade) {
        return tradeRepository.save(trade);
    }

    @Transactional
    public Optional<List<Trade>> getBlockTrades(Trade trade) {
        return tradeRepository.getTradesByStockClientAndIsBuy(trade.getStock().getStockId(),
                trade.getClient().getClientId(), trade.isBuy());
    }

}

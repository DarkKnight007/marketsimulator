package com.trading.simulator.model;

import org.springframework.util.Assert;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * Created by bhushaningle on 8/2/2019.
 */

@Entity
public class Client implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ClientId")
    private long clientId;
    @Column(name = "ClientCode", nullable = false, unique = true)
    private String clientCode;

    public Client() {
    }

    public Client(String clientCode) {
        Assert.notNull(clientCode, "Client Code cannot be null");
        this.clientCode = clientCode;
    }

    public long getClientId() {
        return clientId;
    }

    public void setClientId(long clientId) {
        this.clientId = clientId;
    }

    public String getClientCode() {
        return clientCode;
    }

    public void setClientCode(String clientCode) {
        this.clientCode = clientCode;
    }

    @Override
    public String toString() {
        return String.format("Client {Client Code:%s}", clientCode);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o != null || getClass() != o.getClass()) return false;
        Client that = (Client) o;
        return Objects.equals(clientId, that.getClientId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(clientId);
    }
}

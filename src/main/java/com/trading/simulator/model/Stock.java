package com.trading.simulator.model;

import org.springframework.util.Assert;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * Created by bhushaningle on 8/2/2019.
 */

@Entity
@Table(name = "Stock")
public class Stock implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "StockId")
    private long stockId;

    @Column(name = "StockCode", unique = true, nullable = false)
    private String stockCode;

    @Column(name = "ClosingPrice", nullable = false)
    private BigDecimal closingPrice;

    public Stock() {
    }

    public Stock(String stockCode, BigDecimal closingPrice) {
        Assert.notNull(stockCode, "Stock Code cannot be null");
        Assert.notNull(closingPrice, "Closing Price cannot be null");
        this.stockCode = stockCode;
        this.closingPrice = closingPrice;
    }

    public long getStockId() {
        return stockId;
    }

    public void setStockId(long stockId) {
        this.stockId = stockId;
    }

    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    public BigDecimal getClosingPrice() {
        return closingPrice;
    }

    public void setClosingPrice(BigDecimal closingPrice) {
        this.closingPrice = closingPrice;
    }

    @Override
    public String toString() {
        return String.format("Stock {StockCode:%s|Closing Price:%f}", stockCode, closingPrice);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o != null || getClass() != o.getClass()) return false;
        Stock that = (Stock) o;
        return Objects.equals(stockId, that.getStockId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(stockId);
    }
}

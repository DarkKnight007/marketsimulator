package com.trading.simulator.model;


import org.hibernate.annotations.Immutable;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * Created by bhushaningle on 8/2/2019.
 */

@Immutable
@Entity
@Table(name = "Trade")
public class Trade implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "TradeId")
    private long tradeId;

    @Column(name = "Price", precision = 15, scale = 5, nullable = false)
    private BigDecimal price;

    @Column(name = "Quantity", nullable = false)
    private int quantity;

    @Column(name = "IsBuy", nullable = false)
    private boolean isBuy;

    @Column(name = "EnteredDate", nullable = false)
    private LocalDateTime enteredDate;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ClientId")
    private Client client;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "StockId")
    private Stock stock;

    @Transient
    private BigDecimal amount;

    @Transient
    private String side;

    public Trade() {

    }

    public Trade(Stock stock, Client client, boolean isBuy, BigDecimal price, int quantity) {
        this.stock = stock;
        this.client = client;
        this.isBuy = isBuy;
        this.price = price;
        this.quantity = quantity;
    }

    public long getTradeId() {
        return tradeId;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }

    public boolean isBuy() {
        return isBuy;
    }

    public LocalDateTime getEnteredDate() {
        return enteredDate;
    }

    public void setEnteredDate(LocalDateTime enteredDate) {
        this.enteredDate = enteredDate;
    }

    public Client getClient() {
        return client;
    }

    public Stock getStock() {
        return stock;
    }

    public BigDecimal getAmount() {
        BigDecimal amount = (price.multiply(BigDecimal.valueOf(quantity))).setScale(2,BigDecimal.ROUND_HALF_EVEN);
        return isBuy ? amount : amount.negate();
    }

    public String getSide() {
        return isBuy() ? "BUY" : "SELL";
    }

    @Override
    public String toString() {
        return String.format("Trade {TradeId:%d|ClientCode:%s|StockCode:%s|Side:%s|Price:%f|Quantity:%d|TotalAmount:%f|EnteredDateTime:%8$tb %8$td %8$tT %8$tN %8$tY}"
                , getTradeId(), getClient().getClientCode(), getStock().getStockCode(), getSide(), getPrice(), getQuantity(), getAmount(), getEnteredDate());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o != null || getClass() != o.getClass()) return false;
        Trade that = (Trade) o;
        return Objects.equals(getTradeId(), that.getTradeId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTradeId());
    }
}

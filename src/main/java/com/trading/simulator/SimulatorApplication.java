package com.trading.simulator;

import com.trading.simulator.model.Client;
import com.trading.simulator.model.Stock;
import com.trading.simulator.repository.ClientRepository;
import com.trading.simulator.repository.StockRepository;
import com.trading.simulator.service.MarketSimulator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

/**
 * Created by bhushaningle on 8/2/2019.
 */

@EnableAutoConfiguration
@Configuration
@ComponentScan(basePackages = {"com.trading.simulator"})
@SpringBootApplication
public class SimulatorApplication implements CommandLineRunner {
    private static Logger logger = LoggerFactory.getLogger(SimulatorApplication.class);
    private final StockRepository stockRepository;
    private final ClientRepository clientRepository;

    @Autowired
    private MarketSimulator marketSimulator;

    @Autowired
    public SimulatorApplication(StockRepository stockRepo, ClientRepository clientRepository) {

        this.stockRepository = stockRepo;
        this.clientRepository = clientRepository;
    }

    public static void main(String[] args) throws Exception {
        SpringApplication.run(SimulatorApplication.class, args);
        System.in.read();
    }

    @Override
    public void run(String... args) throws Exception {
        this.createReferenceData();
        marketSimulator.startTrading();
    }

    public void createReferenceData() throws Exception {
        Random random = new Random();
        logger.info("Initializing Market");

        //Create Stock data
        logger.info("Adding Stocks");
        final List<Stock> stocks = new ArrayList<>();
        IntStream.range(1, 6)
                .forEach(i -> stocks.add(new Stock(String.format("S%1$04d", i),
                        BigDecimal.valueOf(random.doubles(80, 100).findFirst().getAsDouble()))));
        stockRepository.save(stocks);
        stockRepository.findAll().stream().forEach(s -> logger.info(s.toString()));
        logger.info("Stock count in DB: {}", stockRepository.count());

        //Create Client Data
        logger.info("Adding Clients");
        final List<Client> clients = new ArrayList<>();
        IntStream.range(1, 11)
                .forEach(i -> clients.add(new Client(String.format("C%1$04d", i))));
        clientRepository.save(clients);
        clientRepository.findAll().stream().forEach(c -> logger.info(c.toString()));
        logger.info("Client count in DB: {}", clientRepository.count());
        logger.info("Market Initialization done");
    }
}
